import React from 'react';
import { View, StyleSheet, Button, TouchableOpacity, Text } from 'react-native';
import { Audio } from 'expo-av';


function AudioComp(props) {
  const [sound, setSound] = React.useState();
  async function playSound() {
    const { sound } = await Audio.Sound.createAsync(
      props.soundUrl
    );
    setSound(sound);
    await sound.playAsync();
  }

  React.useEffect(() => {
    return sound
      ? () => {
        // console.log('Unloading Sound');
        sound.unloadAsync();
      }
      : undefined;
  }, [sound]);

  return (
    <TouchableOpacity
      style={styles.btn}
      onPress={playSound} 
    >
      <Text>{props.auTitle}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  btn: {
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
    width: '25%',
    marginVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  }
});

export default AudioComp;