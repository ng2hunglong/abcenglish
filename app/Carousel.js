import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet, FlatList, Animated } from 'react-native';
import WordData from './WordData';
import VocabCard from './VocabCard';

function Carousel() {
  const scrollX = useRef(new Animated.Value(0)).current;
  
  return (
    <View style={styles.container}>
      <FlatList
        data={WordData}
        renderItem={({ item }) => {
          return <VocabCard {...item}/>
        }}
        horizontal
        showsHorizontalScrollIndicator
        pagingEnabled
        bounces={false}
        keyExtractor={(item) => item.id}
        onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], {
          useNativeDriver: false,
        })}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  }
})

export default Carousel;