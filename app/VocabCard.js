import React from 'react';
import { View, Text, Image, ImageBackground, useWindowDimensions } from 'react-native';
import AudioComp from './AudioComp';

function VocabCard(props) {
  const { width, height } = useWindowDimensions();
  return (
    <ImageBackground
      style={[styles.imgBgr, { width }, { height }]}
      source={props.backgroundImg}
      resizeMode="contain"
    >
      <AudioComp auTitle={props.letter} soundUrl={props.letterUrl}/>
      <AudioComp auTitle={props.word} soundUrl={props.wordUrl}/>
      <AudioComp auTitle={props.spellWord} soundUrl={props.spellUrl}/>
    </ImageBackground>
  );
}

const styles = {
  imgBgr: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
}

export default VocabCard;