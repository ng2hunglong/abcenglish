export default [
  {
    letter: 'a',
    word: 'apple',
    spellWord: 'a\-p\-p\-l\-e',
    letterUrl: require('./audio/a.mp3'),
    wordUrl: require('./audio/apple.mp3'),
    spellUrl: require('./audio/apple-spell.mp3'),
    backgroundImg: require('./image/apple.jpg')
  },
  {
    letter: 'b',
    word: 'banana',
    spellWord: 'b\-a\-n\-a\-n\-a',
    letterUrl: require('./audio/b.mp3'),
    wordUrl: require('./audio/banana.mp3'),
    spellUrl: require('./audio/banana-spell.mp3'),
    backgroundImg: require('./image/banana.jpg')
  },
  {
    letter: 'c',
    word: 'car',
    spellWord: 'c\-a\-r',
    letterUrl: require('./audio/c.mp3'),
    wordUrl: require('./audio/car.mp3'),
    spellUrl: require('./audio/car-spell.mp3'),
    backgroundImg: require('./image/car.jpg')
  },
  {
    letter: 'd',
    word: 'dog',
    spellWord: 'd\-o\-g',
    letterUrl: require('./audio/d.mp3'),
    wordUrl: require('./audio/dog.mp3'),
    spellUrl: require('./audio/dog-spell.mp3'),
    backgroundImg: require('./image/dog.jpg')
  },
  {
    letter: 'e',
    word: 'elephant',
    spellWord: 'e\-l\-e\-p\-h\-a\-n\-t',
    letterUrl: require('./audio/e.mp3'),
    wordUrl: require('./audio/elephant.mp3'),
    spellUrl: require('./audio/elephant-spell.mp3'),
    backgroundImg: require('./image/elephant.jpg')
  },
  {
    letter: 'f',
    word: 'finger',
    spellWord: 'f\-i\-n\-g\-e\-r',
    letterUrl: require('./audio/f.mp3'),
    wordUrl: require('./audio/finger.mp3'),
    spellUrl: require('./audio/finger-spell.mp3'),
    backgroundImg: require('./image/finger.jpg')
  },
  {
    letter: 'g',
    word: 'giraffe',
    spellWord: 'g\-i\-r\-a\-f\-f\-e',
    letterUrl: require('./audio/f.mp3'),
    wordUrl: require('./audio/giraffe.mp3'),
    spellUrl: require('./audio/giraffe-spell.mp3'),
    backgroundImg: require('./image/giraffe.jpg')
  },
  {
    letter: 'h',
    word: 'hotel',
    spellWord: 'h\-o\-t\-e\-l',
    letterUrl: require('./audio/h.mp3'),
    wordUrl: require('./audio/hotel.mp3'),
    spellUrl: require('./audio/hotel-spell.mp3'),
    backgroundImg: require('./image/hotel.jpg')
  },
  {
    letter: 'i',
    word: 'island',
    spellWord: 'i\-s\-l\-a\-n\-d',
    letterUrl: require('./audio/i.mp3'),
    wordUrl: require('./audio/island.mp3'),
    spellUrl: require('./audio/island-spell.mp3'),
    backgroundImg: require('./image/island.jpg')
  },
  {
    letter: 'j',
    word: 'jungle',
    spellWord: 'j\-u\-n\-g\-l\-e',
    letterUrl: require('./audio/j.mp3'),
    wordUrl: require('./audio/jungle.mp3'),
    spellUrl: require('./audio/jungle-spell.mp3'),
    backgroundImg: require('./image/jungle.jpg')
  },
  {
    letter: 'k',
    word: 'kangaroo',
    spellWord: 'k\-a\-n\-g\-a\-r\-o\-o',
    letterUrl: require('./audio/k.mp3'),
    wordUrl: require('./audio/kangaroo.mp3'),
    spellUrl: require('./audio/kangaroo-spell.mp3'),
    backgroundImg: require('./image/kangaroo.jpg')
  },
  {
    letter: 'l',
    word: 'lion',
    spellWord: 'l\-i\-o\-n',
    letterUrl: require('./audio/l.mp3'),
    wordUrl: require('./audio/lion.mp3'),
    spellUrl: require('./audio/lion-spell.mp3'),
    backgroundImg: require('./image/lion.jpg')
  },
  {
    letter: 'm',
    word: 'mother',
    spellWord: 'm\-o\-t\-h\-e\-r',
    letterUrl: require('./audio/m.mp3'),
    wordUrl: require('./audio/mother.mp3'),
    spellUrl: require('./audio/mother-spell.mp3'),
    backgroundImg: require('./image/mother.jpg')
  },
  {
    letter: 'n',
    word: 'night',
    spellWord: 'n\-i\-g\-h\-t',
    letterUrl: require('./audio/n.mp3'),
    wordUrl: require('./audio/night.mp3'),
    spellUrl: require('./audio/night-spell.mp3'),
    backgroundImg: require('./image/night.jpg')
  },
  {
    letter: 'o',
    word: 'oat',
    spellWord: 'o\-a\-t',
    letterUrl: require('./audio/o.mp3'),
    wordUrl: require('./audio/oat.mp3'),
    spellUrl: require('./audio/oat-spell.mp3'),
    backgroundImg: require('./image/oat.jpg')
  },
  {
    letter: 'p',
    word: 'penguin',
    spellWord: 'p\-e\-n\-g\-u\-i\-n',
    letterUrl: require('./audio/p.mp3'),
    wordUrl: require('./audio/penguin.mp3'),
    spellUrl: require('./audio/penguin-spell.mp3'),
    backgroundImg: require('./image/penguin.jpg')
  },
  {
    letter: 'q',
    word: 'quiet',
    spellWord: 'q\-u\-i\-e\-t',
    letterUrl: require('./audio/q.mp3'),
    wordUrl: require('./audio/quiet.mp3'),
    spellUrl: require('./audio/quiet-spell.mp3'),
    backgroundImg: require('./image/quiet.jpg')
  },
  {
    letter: 'r',
    word: 'rabbit',
    spellWord: 'r\-a\-b\-b\-i\-t',
    letterUrl: require('./audio/r.mp3'),
    wordUrl: require('./audio/rabbit.mp3'),
    spellUrl: require('./audio/rabbit-spell.mp3'),
    backgroundImg: require('./image/rabbit.jpg')
  },
  {
    letter: 's',
    word: 'seven',
    spellWord: 's\-e\-v\-e\-n',
    letterUrl: require('./audio/s.mp3'),
    wordUrl: require('./audio/seven.mp3'),
    spellUrl: require('./audio/seven-spell.mp3'),
    backgroundImg: require('./image/seven.jpg')
  },
  {
    letter: 't',
    word: 'tea',
    spellWord: 't\-e\-a',
    letterUrl: require('./audio/t.mp3'),
    wordUrl: require('./audio/tea.mp3'),
    spellUrl: require('./audio/tea-spell.mp3'),
    backgroundImg: require('./image/tea.jpg')
  },
  {
    letter: 'u',
    word: 'under',
    spellWord: 'u\-n\-d\-e\-r',
    letterUrl: require('./audio/u.mp3'),
    wordUrl: require('./audio/under.mp3'),
    spellUrl: require('./audio/under-spell.mp3'),
    backgroundImg: require('./image/under.jpg')
  },
  {
    letter: 'v',
    word: 'vacation',
    spellWord: 'v\-a\-c\-a\-t\-i\-o\-n',
    letterUrl: require('./audio/v.mp3'),
    wordUrl: require('./audio/vacation.mp3'),
    spellUrl: require('./audio/vacation-spell.mp3'),
    backgroundImg: require('./image/vacation.jpg')
  },
  {
    letter: 'w',
    word: 'white',
    spellWord: 'w\-h\-i\-t\-e',
    letterUrl: require('./audio/w.mp3'),
    wordUrl: require('./audio/white.mp3'),
    spellUrl: require('./audio/white-spell.mp3'),
    backgroundImg: require('./image/white.jpg')
  },
  {
    letter: 'x',
    word: 'xmas',
    spellWord: 'x\-m\-a\-s',
    letterUrl: require('./audio/x.mp3'),
    wordUrl: require('./audio/xmas.mp3'),
    spellUrl: require('./audio/xmas-spell.mp3'),
    backgroundImg: require('./image/xmas.jpg')
  },
  {
    letter: 'y',
    word: 'yellow',
    spellWord: 'y\-e\-l\-l\-o\-w',
    letterUrl: require('./audio/y.mp3'),
    wordUrl: require('./audio/yellow.mp3'),
    spellUrl: require('./audio/yellow-spell.mp3'),
    backgroundImg: require('./image/yellow.jpg')
  },
  {
    letter: 'z',
    word: 'zebra',
    spellWord: 'z\-e\-b\-r\-a',
    letterUrl: require('./audio/z.mp3'),
    wordUrl: require('./audio/zebra.mp3'),
    spellUrl: require('./audio/zebra-spell.mp3'),
    backgroundImg: require('./image/zebra.jpg')
  },
]